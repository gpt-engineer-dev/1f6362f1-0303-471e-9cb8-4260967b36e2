document.getElementById('task-form').addEventListener('submit', function(e) {
    e.preventDefault();

    // Get the task input value
    let taskInput = document.getElementById('task-input');
    let taskValue = taskInput.value;

    // Create a new list item
    let li = document.createElement('li');
    li.className = 'border-gray-400 flex flex-row mb-2';
    li.innerHTML = `
        <div class="select-none cursor-pointer bg-white rounded-md flex flex-1 items-center p-4">
            <div class="flex-1 pl-1 mr-16">
                <div class="font-medium">${taskValue}</div>
            </div>
            <div class="text-red-600 hover:text-red-900 cursor-pointer">
                <i class="fa fa-trash-alt"></i>
            </div>
        </div>
    `;

    // Add the new task to the task list
    let taskList = document.getElementById('task-list');
    taskList.appendChild(li);

    // Clear the task input value
    taskInput.value = '';

    // Add event listener to the delete button
    li.querySelector('.fa-trash-alt').addEventListener('click', function() {
        taskList.removeChild(li);
    });
});
